package com.file;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class FileVerification {
			private String value;
		 public static void deleteFiles()
		    {
		        int numOfMonths = -6;
		        String path="E:\\GIT\\TestGit";
		        File file = new File(path);
		        FileHandler fh;
		        Calendar sixMonthAgo = Calendar.getInstance();
		        Calendar currentDate = Calendar.getInstance();
		        Logger logger = Logger.getLogger("MyLog");
		        sixMonthAgo.add(Calendar.MONTH, numOfMonths);
		        File[] files = file.listFiles();
		        ArrayList<String> arrlist = new ArrayList<String>();

		        try {
		            fh = new FileHandler("G:\\Files\\logFile\\MyLogForDeletedFile.log");
		            logger.addHandler(fh);
		            SimpleFormatter formatter = new SimpleFormatter();
		            fh.setFormatter(formatter);

		            for (File f:files)
		            {
		                if (f.isFile() && f.exists())
		                {
		                    Date lastModDate = new Date(f.lastModified());
		                    if(lastModDate.before(sixMonthAgo.getTime()))
		                    {
		                        arrlist.add(f.getName());
		                        f.delete();
		                    }
		                }
		            }
		            for(int i=0;i<arrlist.size();i++)
		                logger.info("deleted files are ===>"+arrlist.get(i));
		        }
		        catch ( Exception e ){
		            e.printStackTrace();
		            logger.info("error is-->"+e);
		        }
		    }
		    public static void main(String[] args)
		    {
		        deleteFiles();
		    }
		}

	


