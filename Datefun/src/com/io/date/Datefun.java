package com.io.date;

import java.time.DateTimeException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

public class Datefun {
public static void main(String[] args) throws DateTimeException {
	 LocalDateTime.now();
	 
	 System.out.println( LocalDateTime.now());
	 LocalDate date1 = LocalDate.now();
     System.out.println("Current date: " + date1);
	 LocalDate nextTuesday = date1.with(TemporalAdjusters.previous(DayOfWeek.TUESDAY));
     System.out.println("previous Tuesday on : " + nextTuesday);
     
     DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm a");  
     
}
}
